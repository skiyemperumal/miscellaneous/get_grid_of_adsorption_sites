from ase import Atom
from ase.io import read, write
import numpy as np
import os

def gen_io_files():
    for i, j in zip(x,z):
        s.append(Atom('Cu', (i, s[ref1].y, j)))
        write('%.2f-%.2f.xyz'%(i,j), s)
        del s[[atom.index for atom in s if atom.symbol=='Cu']]
        os.system('mkdir %.2f-%.2f; cp in %.2f-%.2f; mv %.2f-%.2f.xyz %.2f-%.2f/a.xyz'%(i,j,i,j,i,j,i,j))

def read_energy():
    with open('out','r') as f:
        for line in f: 
            if 'GEOMETRY OPTIMIZATION COMPLETED' in line:
               for lineE in f:
                   if 'ENERGY|' in lineE:
                       energy = lineE.strip().split()[-1]
               return energy
            #else:
            #    print os.getcwd(), 'NOT CONVERGED'

s = read('a_tio2.xyz')

#Choose reference atoms for defining origin of the fragment of XY plane in the unit cell and
#add an atom that is 2.0 Angst above z coordinate of ref1 atom
ref1 = 44
ref2 = 140
refZ = s[ref1].z + 2.0
#lattice constants of the unit cell
cell = [20.5975532527531051,  15.1199999999999992,  30.0000000000000000]

s.set_cell(cell)
# resolution of 0.1 Angst seems to cover all most relevent sites of adsorption
xpoints = np.arange(s[ref1].x, s[ref2].x, 0.1) 

#Since TiO2 contains sites where an adatom can adsorb with different z values, 
#I am creating fragments with decreasing/increasing z value from one site to another
x1 = np.arange(s[ref1].x, s[214].x, 0.1)
z1 = np.linspace(s[ref1].z + 1.9, s[214].z +2.6, len(x1))

x2 = np.arange(s[214].x, s[47].x, 0.1)
z2 = np.linspace(s[214].z + 2.6, s[47].z + 2.0, len(x2))

x3 = np.arange(s[47].x, (s[47].x+s[43].x)/2, 0.1)
z3 = np.linspace(s[47].z + 2.0, s[69].z + 0.6, len(x3))

x4 = np.arange((s[47].x+s[43].x)/2, s[43].x, 0.1)
z4 = np.linspace(s[69].z + 0.6, s[43].z + 1.9, len(x4))

x5 = np.arange(s[43].x, s[261].x, 0.1)
z5 = np.linspace(s[43].z + 1.9, s[261].z + 2.5, len(x5))

x6 = np.arange(s[261].x, s[140].x, 0.1)
z6 = np.linspace(s[261].z + 2.5, s[140].z + 1.9, len(x6))

#concating list requires array to list conversion
x = list(x1) + list(x2) + list(x3) + list(x4) + list(x5) + list(x6)
z = list(z1) + list(z2) + list(z3) + list(z4) + list(z5) + list(z6)

print len(x), len(z)
gen_io_files()

'''
###############Post Processing####################
for j in ypoints:
    for i in xpoints:
        try:
            os.chdir('%.2f-%.2f'%(i,j))
            print read_energy(), '%.2f-%.2f'%(i,j) 
            os.chdir('../')
        except IOError:
            print '%.2f-%.2f'%(i,j)
            os.chdir('../')
'''
